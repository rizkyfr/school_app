import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';

class WIP extends StatefulWidget {
  @override
  _WIPState createState() => _WIPState();
}

class _WIPState extends State<WIP> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.backAndTitle(context, "WIP"),
      body: Container()
    );
  }
}
