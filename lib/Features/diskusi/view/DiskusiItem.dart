import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/home/viewmodel/HomeViewModel.dart';

class DiskusiItem extends StatefulWidget {
  final List<DiskusiContent> viewContents;

  DiskusiItem(this.viewContents);
  @override
  State createState() {
    return _DiskusiItemComponent();
  }
}

class _DiskusiItemComponent extends State<DiskusiItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widget.viewContents
            .asMap()
            .map(
              (i, e) {
                return MapEntry(
                  i,
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(e.category, style: TextStyle(fontSize: 12, color: CustomColor.cRed(), fontWeight: FontWeight.w700)),
                            SizedBox(height: 10),
                            Text(e.title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                            SizedBox(height: 10),
                            Text(e.content, style: TextStyle(fontSize: 13)),
                            SizedBox(height: 10),
                            Row(
                              children: [
                                SvgPicture.asset("assets/images/ic_diskusimrh.svg"),
                                SizedBox(width: 5),
                                Text(e.totalMsg, style: TextStyle(fontSize: 13)),
                                SizedBox(width: 15),
                                Text(e.timesMsg, style: TextStyle(fontSize: 13)),
                              ],
                            ),
                            SizedBox(height: 10),
                            Row(
                              children: [
                                ClipRRect(
                                  clipBehavior: Clip.hardEdge,
                                  borderRadius: BorderRadius.circular(20),
                                  child: Container(
                                    color: Color(0xffdddddd),
                                    child: Image.asset(
                                      "assets/images/home_logodummy.png",
                                      width: 32,
                                      height: 32,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Text(
                                    e.user,
                                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ],
                            ),
                            if (i < widget.viewContents.length - 1)
                              Padding(
                                padding: const EdgeInsets.only(left: 0, right: 0, top: 25, bottom: 25),
                                child: SizedBox(
                                  height: 1,
                                  width: MediaQuery.of(context).size.width,
                                  child: DecoratedBox(
                                    decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            )
            .values
            .toList(),
      ),
    );
  }
}
