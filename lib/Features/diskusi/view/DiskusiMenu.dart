import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/diskusi/view/DiskusiItem.dart';
import 'package:school_app/Features/home/viewmodel/HomeViewModel.dart';

class DiskusiMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DiskusiMenuState();
}

class _DiskusiMenuState extends State<DiskusiMenu> {
  int _value = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<DiskusiContent> diskusi = [
      DiskusiContent(
          category: "PEMBAYARAN",
          title: "Lorem ipsum dolor sit amet, consecte adipiscing elit?",
          content:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis magna suspendisse integer orci. Pulvinar quam scelerisque urna sodales eget. Tem...",
          totalMsg: "0",
          timesMsg: "15 Menit yang lalu",
          user: "Alif Frederico"),
      DiskusiContent(
          category: "TUGAS",
          title: "Lorem ipsum dolor sit amet?",
          content:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis magna suspendisse integer orci. Pulvinar quam scelerisque urna sodales eget. Tem...",
          totalMsg: "1",
          timesMsg: "16 Menit yang lalu",
          user: "Ajeng Firly"),
    ];
    List<DiskusiContent> diskusiPop = [
      DiskusiContent(
          category: "PEMBAYARANPOP",
          title: "Lorem ipsum dolor sit amet, consecte adipiscing elit?",
          content:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis magna suspendisse integer orci. Pulvinar quam scelerisque urna sodales eget. Tem...",
          totalMsg: "0",
          timesMsg: "15 Menit yang lalu",
          user: "Alif Frederico"),
      DiskusiContent(
          category: "TUGASPOP",
          title: "Lorem ipsum dolor sit amet?",
          content:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lobortis magna suspendisse integer orci. Pulvinar quam scelerisque urna sodales eget. Tem...",
          totalMsg: "1",
          timesMsg: "16 Menit yang lalu",
          user: "Ajeng Firly"),
    ];
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.only(top: 15, bottom: 20, right: 15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 3.0,
                      height: 30.0,
                      child: DecoratedBox(
                        decoration: BoxDecoration(color: CustomColor.cGreen()),
                      ),
                    ),
                    SizedBox(width: 12),
                    Expanded(
                      child: Text(
                        "Diskusi",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CustomColor.cGreyText()),
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        "Buat Diskusi",
                        style: TextStyle(color: CustomColor.cRed(), fontSize: 13, fontWeight: FontWeight.w600),
                      ),
                      onTap: () {},
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      child: Text(
                        "Lihat Semua",
                        style: TextStyle(color: CustomColor.cRed(), fontSize: 13, fontWeight: FontWeight.w600),
                      ),
                      onTap: () {},
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        decoration: _value == 0
                            ? BoxDecoration(borderRadius: BorderRadius.circular(30), color: CustomColor.cBackgroundRed())
                            : BoxDecoration(
                                borderRadius: BorderRadius.circular(30), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              _value = 0;
                            });
                          },
                          child: Text(
                            "Terbaru",
                            style: TextStyle(color: _value == 0 ? CustomColor.cRed() : Colors.black),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        decoration: _value == 1
                            ? BoxDecoration(borderRadius: BorderRadius.circular(30), color: CustomColor.cBackgroundRed())
                            : BoxDecoration(
                                borderRadius: BorderRadius.circular(30), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              _value = 1;
                            });
                          },
                          child: Text(
                            "Popular",
                            style: TextStyle(color: _value == 1 ? CustomColor.cRed() : Colors.black),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: SizedBox(
                    height: 1,
                    width: MediaQuery.of(context).size.width,
                    child: DecoratedBox(
                      decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
                    ),
                  ),
                ),
                DiskusiItem(diskusi),
              ],
            ),
          ),
        ],
      ),
    );
  }
}