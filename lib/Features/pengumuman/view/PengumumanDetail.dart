import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class PengumumanDetail extends StatefulWidget {
  final String imageCover;
  final String title;
  final String date;
  final String desc;

  const PengumumanDetail({required this.title, required this.date, required this.imageCover, required this.desc});

  @override
  _ArtikelDetail createState() => _ArtikelDetail();
}

class _ArtikelDetail extends State<PengumumanDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.backAndTitle(context, "Pengumuman"),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 10),
                      child: Text(
                        widget.title,
                        style: TextStyle(fontSize: 21, color: CustomColor.cGreyText(), fontWeight: FontWeight.w700),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
                      child: Text(
                        widget.date,
                        style: TextStyle(fontSize: 14, color: CustomColor.cGreyText()),
                      ),
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child: Image(
                        image: AssetImage("${widget.imageCover}"),
                        height: 185,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, top: 20, right: 15, bottom: 15),
                      child: Text(
                        widget.desc,
                        style: TextStyle(height: 1.5, fontSize: 16, color: CustomColor.cGreyText()),
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
