import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/pengumuman/view/PengumumanDetail.dart';
import 'package:school_app/Features/pengumuman/viewmodel/PengumumanViewModel.dart';

class PengumumanComponent extends StatefulWidget {
  final List<PengumumanItemContent> viewContents;

  const PengumumanComponent({required this.viewContents});

  @override
  State createState() {
    return _PengumumanComponent();
  }
}

class _PengumumanComponent extends State<PengumumanComponent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
          child: Column(
            children: widget.viewContents
                .asMap()
                .map(
                  (i, e) {
                    return MapEntry(
                      i,
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PengumumanDetail(
                                    imageCover: widget.viewContents[i].image,
                                    title: widget.viewContents[i].title,
                                    date: widget.viewContents[i].date,
                                    desc: widget.viewContents[i].desc),
                              ),
                            ),
                            child: Column(
                              children: [
                                itemPengumuman(e.image, e.title, e.date, e.desc),
                                if (i < widget.viewContents.length - 1)
                                  Padding(
                                    padding: const EdgeInsets.only(left: 0, right: 0, top: 25, bottom: 25),
                                    child: SizedBox(
                                      height: 1,
                                      width: MediaQuery.of(context).size.width,
                                      child: DecoratedBox(
                                        decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                )
                .values
                .toList(),
          ),
        ),
      ],
    );
  }

  Widget itemPengumuman(String image, String title, String date, String desc) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ClipRRect(
          clipBehavior: Clip.hardEdge,
          borderRadius: BorderRadius.circular(5),
          child: Image(
            height: 143,
            width: 125,
            fit: BoxFit.cover,
            image: AssetImage(image),
          ),
        ),
        SizedBox(width: 15),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                strutStyle: StrutStyle(fontSize: 12.0),
                text: TextSpan(
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CustomColor.cGreyText()),
                  text: title,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(date, textAlign: TextAlign.left, style: TextStyle(fontSize: 13, color: CustomColor.cGreyText())),
              ),
              RichText(
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                strutStyle: StrutStyle(fontSize: 12.0),
                text: TextSpan(
                  style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()),
                  text: desc,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
