import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/home/view/BannerView.dart';
import 'package:school_app/Features/pengumuman/view/PengumumanComponent.dart';
import 'package:school_app/Features/pengumuman/viewmodel/PengumumanViewModel.dart';

class Pengumuman extends StatefulWidget {
  @override
  _PengumumanState createState() => _PengumumanState();
}

class _PengumumanState extends State<Pengumuman> {
  List<PengumumanItemContent> pengumumanContent = [
    PengumumanItemContent(
        image: "assets/images/img_dummy_2_detail.png",
        title: "PEMBELAJARAN TATAP MUKA (PTM)",
        date: "Senin, 7 Des 2021",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis eu volutpat odio facilisis mauris sit amet massa vitae tortor condimentum lacinia quis vel eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus, viverra vitae congue eu, consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus",
       ),
    PengumumanItemContent(
        image: "assets/images/img_dummy_2_detail.png",
        title: "LIPUTAN JAWAPOS 4 JUNI 2021 “PEMBELAJARAN DA...",
        date: "Senin, 7 Des 2021",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis eu volutpat odio facilisis mauris sit amet massa vitae tortor condimentum lacinia quis vel eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus, viverra vitae congue eu, consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus",
        ),
    PengumumanItemContent(
        image: "assets/images/img_dummy_2_detail.png",
        title: "JADWAL 31 MEI- 5 JUNI 2021",
        date: "Senin, 7 Des 2021",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis eu volutpat odio facilisis mauris sit amet massa vitae tortor condimentum lacinia quis vel eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus, viverra vitae congue eu, consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus",
       ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.cBackgroundGrey(),
      appBar: CustomAppBar.titleAndNotif("School App"),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                child: BannerView(height: 145),
              ),
              Container(
                color: Colors.white,
                child: Column(
                  children: [
                    PengumumanComponent(viewContents: pengumumanContent),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
