class PengumumanItemContent {
  final String image;
  final String title;
  final String date;
  final String desc;

  const PengumumanItemContent({required this.image, required this.title, required this.date, required this.desc,});
}
