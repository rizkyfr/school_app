import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/dokumen/view/DokumenRowComponent.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';
import 'package:school_app/WIP.dart';

class Dokumen extends StatefulWidget {
  @override
  State createState() => _DokumenState();
}

class _DokumenState extends State<Dokumen> {
  List<ProfileViewRowContent> dokumen = [
    ProfileViewRowContent(name: "Ijazah", widget: WIP(),),
    ProfileViewRowContent(name: "Data Raport", widget: WIP(), ),
    ProfileViewRowContent(name: "Akta Kelahiran", widget: WIP(),),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.cBackgroundGrey(),
      appBar: CustomAppBar.backAndTitle(context, "Dokumen"),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              SizedBox(height: 15),
              DokumenRowComponent(viewContents: dokumen),
              SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }
}
