import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';

class DokumenRowComponent extends StatefulWidget {
  final List<ProfileViewRowContent> viewContents;

  DokumenRowComponent({required this.viewContents});

  @override
  State createState() {
    return _DokumenRowComponent();
  }
}

class _DokumenRowComponent extends State<DokumenRowComponent> {
  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12, right: 12, top: 20, bottom: 20),
            child: Column(
              children: widget.viewContents
                  .asMap()
                  .map(
                    (i, e) {
                      return MapEntry(
                        i,
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => e.widget)),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          widget.viewContents[i].name,
                                          style: TextStyle(fontSize: 16, color: CustomColor.cGreyText()),
                                        ),
                                      ),
                                      Icon(Icons.arrow_forward, color: CustomColor.cRed()),
                                    ],
                                  ),
                                  if (i < widget.viewContents.length - 1)
                                    Padding(
                                      padding: const EdgeInsets.only(left: 0, right: 0, top: 25, bottom: 25),
                                      child: SizedBox(
                                        height: 1,
                                        width: MediaQuery.of(context).size.width,
                                        child: DecoratedBox(
                                          decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  )
                  .values
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
