class DiskusiContent {
  final String category;
  final String title;
  final String content;
  final String totalMsg;
  final String timesMsg;
  final String user;

  DiskusiContent(
      {required this.category, required this.title, required this.content, required this.totalMsg, required this.timesMsg, required this.user});
}
