import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class NotifStatusBayaranPage extends StatefulWidget {
  final String status;

  NotifStatusBayaranPage({required this.status});

  @override
  _NotifStatusBayaranPageState createState() => _NotifStatusBayaranPageState();
}

class _NotifStatusBayaranPageState extends State<NotifStatusBayaranPage> {
  bool _isVisible = true;
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: _isVisible,
      child: Container(
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          color: widget.status == "tidak" ? CustomColor.cBackgroundRed() : CustomColor.cBackgroundGreen(),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset(widget.status == "ya" ? "assets/images/ic_done.svg" : "assets/images/ic_warning.svg", alignment: Alignment.topLeft),
            SizedBox(width: 10),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 12, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.status == "tidak" ? "SPP Bulanan" : "Terima Kasih",
                        style: TextStyle(fontSize: 17, color: CustomColor.cGreyText(), fontWeight: FontWeight.w700)),
                    SizedBox(height: 4),
                    Text(
                        widget.status == "tidak"
                            ? "Segera lakukan pembayaran SPP untuk bulan September"
                            : "Pembayaran SPP bulan September sudah terbayarkan",
                        style: TextStyle(fontSize: 14, color: CustomColor.cGreyText())),
                  ],
                ),
              ),
            ),
            widget.status == "tidak"
                ? Container(
                    padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                    child: TextButton(
                      onPressed: () => null,
                      child: Text("Bayar", style: TextStyle(fontSize: 13, color: Colors.white, fontWeight: FontWeight.w600)),
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.only(top: 10, right: 15, bottom: 10, left: 15)),
                        backgroundColor: MaterialStateProperty.all<Color>(CustomColor.cRed()),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                    ),
                  )
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.status == "ya" ? _isVisible = false : _isVisible = true;
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.only(right: 10, top: 10),
                      child: SvgPicture.asset(
                        widget.status == "ya" ? "assets/images/ic_close.svg" : "assets/images/ic_warning.svg",
                        alignment: Alignment.topRight,
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
