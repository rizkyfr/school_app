import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_app/Domainless/View/CustomButton.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class AbsensiMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AbsensiMenuState();
}

class _AbsensiMenuState extends State<AbsensiMenu> {
  int _value = 0;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Absensi Kehadiran Siswa", style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w700)),
                    SizedBox(height: 5),
                    Text("Senin, 26 April 2021", style: TextStyle(fontSize: 14, color: CustomColor.cGreyText())),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      child: SizedBox(
                        height: 1,
                        width: MediaQuery.of(context).size.width,
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
                        ),
                      ),
                    ),
                    Text("Nama Siswa", style: TextStyle(fontSize: 14, color: CustomColor.cGreyText())),
                    SizedBox(height: 8),
                    Text("Alfred Fahd", style: TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.w700)),
                    SizedBox(height: 30),
                    Text("Status Kehadiran", style: TextStyle(fontSize: 14, color: CustomColor.cGreyText())),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 1;
                              });
                            },
                            child: Row(
                              children: [
                                _value == 1
                                    ? SvgPicture.asset("assets/images/ic_radio_active.svg")
                                    : SvgPicture.asset("assets/images/ic_radio_off.svg"),
                                SizedBox(width: 15),
                                Text("Hadir/Mengikuti Kelas", style: TextStyle(fontWeight: FontWeight.w700)),
                              ],
                            ),
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 2;
                              });
                            },
                            child: Row(
                              children: [
                                _value == 2
                                    ? SvgPicture.asset("assets/images/ic_radio_active.svg")
                                    : SvgPicture.asset("assets/images/ic_radio_off.svg"),
                                SizedBox(width: 15),
                                Text("Izin", style: TextStyle(fontWeight: FontWeight.w700)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    _value == 2 ? izinContent() : Container(),
                    SizedBox(height: 30),
                    ButtonStatic(
                      text: "Kirim",
                      color: CustomColor.cRed(),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                    SizedBox(height: 12),
                    ButtonStaticOutline(
                      text: "Absen Nanti",
                      onTap: () {
                        Navigator.pop(context);
                      }, color: CustomColor.cGreyText(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget izinContent() {
    return Center(
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                maxLines: 4,
                decoration: InputDecoration.collapsed(hintText: "Masukkan keterangan"),
              ),
            ),
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Icon(
                      Icons.note_add_sharp,
                      color: Colors.grey,
                      size: 40,
                    )),
              ),
              SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Berkas Pendukung", style: TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.w700)),
                  SizedBox(height: 5),
                  Text("Upload berkas pendukung\nseperti surat dokter jika sakit", style: TextStyle(fontSize: 14, color: CustomColor.cGreyText())),
                  SizedBox(height: 10),
                  Text("Upload Berkas", style: TextStyle(fontSize: 14, color: CustomColor.cRed(), fontWeight: FontWeight.w500)),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
