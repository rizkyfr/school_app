import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_app/Features/home/view/HomePage.dart';
import 'package:school_app/Features/pembayaran/view/Pembayaran.dart';
import 'package:school_app/Features/pengumuman/view/Pengumuman.dart';
import 'package:school_app/Features/profile/view/Profile.dart';

class HomeTabPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeTabPageState();
}

class HomeTabPageState extends State<HomeTabPage> {
  List<Widget> tabs = <Widget>[
    HomePage(),
    Pengumuman(),
    Pembayaran(),
    Profile(),
  ];
  int selectedTab = 0;
  bool isBottomBarHidden = false;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs.elementAt(selectedTab),
      bottomNavigationBar: isBottomBarHidden
          ? Container(
              height: 0,
            )
          : BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                  icon: SvgPicture.asset("assets/images/ic_beranda.svg"),
                  activeIcon: SvgPicture.asset(
                    "assets/images/ic_beranda_active.svg",
                  ),
                  title: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "Beranda",
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset("assets/images/ic_pengumuman.svg"),
                  activeIcon: SvgPicture.asset("assets/images/ic_pengumuman_active.svg"),
                  title: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "Pengumuman",
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset("assets/images/ic_pembayaran.svg"),
                  activeIcon: SvgPicture.asset(
                    "assets/images/ic_pembayaran_active.svg",
                  ),
                  title: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "Pembayaran",
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset("assets/images/ic_profil.svg"),
                  activeIcon: SvgPicture.asset(
                    "assets/images/ic_profil_active.svg",
                  ),
                  title: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "Profile Murid",
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ),
              ],
              currentIndex: selectedTab,
              selectedItemColor: CustomColor.cRed(),
              selectedFontSize: 12,
              onTap: (tabIndex) {
                setState(
                  () {
                    this.selectedTab = tabIndex;
                  },
                );
              },
            ),
    );
  }
}
