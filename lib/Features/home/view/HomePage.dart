import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/diskusi/view/DiskusiMenu.dart';
import 'package:school_app/Features/home/view/AksesMenuPage.dart';
import 'package:school_app/Features/home/view/BannerView.dart';
import 'package:school_app/Features/home/view/NotifStatusBayaranPage.dart';
import 'package:school_app/Features/jadwalpelajaran/view/JadwalPelajaran.dart';
import 'package:school_app/Features/profile/view/ProfileHeaderComponent.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';
import 'package:school_app/Features/tugas/view/TugasMenu.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.cBackgroundGrey(),
      appBar: CustomAppBar.titleAndNotif("School App"),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ProfileHeaderComponent(
                profileHeaderViewModel: ProfileHeaderViewModel(
                    image: "assets/images/home_logodummy.png",
                    name: "Aflred Fahd",
                    school: "SMA Keluarga 11 Jakarta",
                    grade: "Kelas 11 IPA B",
                    semester: "Semester Ganjil"),
              ),
              SizedBox(height: 20),
              NotifStatusBayaranPage(
                status: "ya",
              ),
              TugasMenu(),
              menuItem(),
              BannerView(height: 145),
              DiskusiMenu(),
            ],
          ),
        ),
      ),
    );
  }

  Widget menuItem() {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(top: 15, bottom: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 3.0,
                height: 30.0,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: CustomColor.cRed()),
                ),
              ),
              SizedBox(width: 12),
              Text("Akses Menu", style: TextStyle(fontSize: 16, color: CustomColor.cGreyText(), fontWeight: FontWeight.w700))
            ],
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => JadwalPelajaran()));
                  },
                  child: Column(
                    children: [
                      SvgPicture.asset("assets/images/ic_jadwal.svg", height: 55, width: 55),
                      SizedBox(height: 8),
                      Text("Jadwal\nPelajaran", textAlign: TextAlign.center, style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    children: [
                      SvgPicture.asset("assets/images/ic_tugas.svg", height: 55, width: 55),
                      SizedBox(height: 8),
                      Text("Tugas", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    children: [
                      SvgPicture.asset("assets/images/ic_diskusi.svg", height: 55, width: 55),
                      SizedBox(height: 8),
                      Text("Diskusi", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    showModalBottomSheet<void>(
                      context: context,
                      isScrollControlled: true,
                      builder: (context) {
                        return AksesMenuPage();
                      },
                    );
                  },
                  child: Column(
                    children: [
                      SvgPicture.asset("assets/images/ic_lainnya.svg", height: 55, width: 55),
                      SizedBox(height: 8),
                      Text("Lainnya", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
