import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/home/view/AbsensiMenu.dart';
import 'package:school_app/Features/jadwalpelajaran/view/JadwalPelajaran.dart';
import 'package:school_app/Features/kalenderakademik/view/KalenderAkademik.dart';

class AksesMenuPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Container(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Akses Menu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_close.svg"),
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 250,
              padding: EdgeInsets.only(top: 10),
              child: GridView.count(
                crossAxisCount: 4,
                childAspectRatio: (1.0),
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => JadwalPelajaran()));
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_jadwal.svg", height: 55, width: 55),
                        SizedBox(height: 8),
                        Text("Jadwal\nPelajaran", textAlign: TextAlign.center, style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_tugas.svg", height: 55, width: 55),
                        SizedBox(height: 8),
                        Text("Tugas", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_diskusi.svg", height: 55, width: 55),
                        SizedBox(height: 8),
                        Text("Diskusi", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(contentPadding: const EdgeInsets.all(0.0), content: new AbsensiMenu());
                          });
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_absensi.svg", height: 55, width: 55),
                        SizedBox(height: 8),
                        Text("Absensi", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => KalenderAkademik()));
                    },
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_kalendar.svg", height: 55, width: 55),
                        SizedBox(height: 8),
                        Text("Kalender\nAkademik", textAlign: TextAlign.center, style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        SvgPicture.asset("assets/images/ic_tentangsekolah.svg", height: 55, width: 55),
                        SizedBox(height: 8),
                        Text("Tentang\nSekolah", style: TextStyle(fontSize: 13, color: CustomColor.cGreyText()))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
