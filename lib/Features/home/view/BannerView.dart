import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class BannerView extends StatefulWidget {
  final double height;

  BannerView({required this.height});

  @override
  State<StatefulWidget> createState() => _BannerViewState();
}

class _BannerViewState extends State<BannerView> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    List<String> images = ["assets/images/home_bannerdummy.png", "assets/images/home_bannerdummy.png", "assets/images/home_bannerdummy.png"];

    List<Widget> imagess = [];
    images.forEach(
      (item) {
        imagess.add(
          Image(
            height: widget.height,
            fit: BoxFit.fill,
            image: AssetImage(item),
          ),
        );
      },
    );
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: ClipRRect(
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(5),
        child: SizedBox(
          height: widget.height,
          child: Stack(
            children: <Widget>[
              PageView(
                children: imagess,
                onPageChanged: (page) {
                  setState(() {
                    this.currentPage = page;
                  });
                },
              ),
              images.length > 0
                  ? Container(
                      padding: EdgeInsets.only(bottom: 5),
                      alignment: Alignment.bottomCenter,
                      child: DotsIndicator(
                        dotsCount: images.length,
                        position: currentPage.toDouble(),
                        decorator: DotsDecorator(
                            size: Size.square(7), activeColor: CustomColor.cRed(), color: CustomColor.cBackgroundGrey(), spacing: EdgeInsets.all(4)),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
