import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/tugas/view/TugasItem.dart';

class TugasMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TugasMenuState();
}

class _TugasMenuState extends State<TugasMenu> {
  int currentPage = 0;
  @override
  Widget build(BuildContext context) {
    List<Tugas> tugas = [
      Tugas(title: "Matematika", desc: "Kalkulus Trigonometri Dasar"),
      Tugas(title: "Bahasa Indonesia", desc: "Bahasa Indonesia"),
    ];
    List<Widget> tugasView = [];
    tugas.forEach(
      (item) {
        tugasView.add(TugasItem(item.title, item.desc));
      },
    );
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(top: 15, bottom: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 3.0,
                      height: 30.0,
                      child: DecoratedBox(
                        decoration: BoxDecoration(color: CustomColor.cBlue()),
                      ),
                    ),
                    SizedBox(width: 12),
                    Expanded(
                      child: Row(
                        children: [
                          Text(
                            "Tugas (${tugas.length})",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CustomColor.cGreyText()),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: GestureDetector(
                        child: Text(
                          "Lihat Semua",
                          style: TextStyle(color: CustomColor.cRed(), fontSize: 13, fontWeight: FontWeight.w600),
                        ),
                        onTap: () {},
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 85,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: PageView(
                          children: tugasView,
                          onPageChanged: (page) {
                            setState(() {
                              currentPage = page;
                            });
                          },
                        ),
                      ),
                      tugas.length > 0
                          ? Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.bottomCenter,
                              child: DotsIndicator(
                                dotsCount: tugas.length,
                                position: currentPage.toDouble(),
                                decorator: DotsDecorator(
                                    size: Size.square(7), activeColor: CustomColor.cRed(), color: CustomColor.cBackgroundGrey(), spacing: EdgeInsets.all(4)),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Tugas {
  String title;
  String desc;
  Tugas({required this.title, required this.desc});
}
