import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class TugasItem extends StatelessWidget {
  final String title;
  final String content;

  TugasItem(this.title, this.content);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      decoration: BoxDecoration(
        color: CustomColor.cBackgroundBlue(),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset("assets/images/ic_download.svg", alignment: Alignment.topLeft),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700)),
                Text(content, style: TextStyle(fontSize: 14)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: GestureDetector(
              onTap: () {},
              child: Column(
                children: [
                  SvgPicture.asset("assets/images/ic_downloadbiru.svg"),
                  Text("Download", style: TextStyle(fontSize: 9, color: CustomColor.cBlue())),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
