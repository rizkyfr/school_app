import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/biodata/view/BiodataRowComponent.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';

class Biodata extends StatefulWidget {
  final List<BiodataViewModel> datas = [
    BiodataViewModel(title: "Nama Siswa", subtitle: "Alfred Fahd"),
    BiodataViewModel(title: "Tempat Lahir", subtitle: "Cianjur"),
    BiodataViewModel(title: "Tanggal Lahir", subtitle: "20-12-2002"),
    BiodataViewModel(title: "Jenis Kelamin", subtitle: "Laki-laki"),
    BiodataViewModel(title: "Nama Ayah", subtitle: "Alvarendra Fahd"),
    BiodataViewModel(title: "Nama Ibu", subtitle: "Alsheiraz Fasa"),
    BiodataViewModel(title: "Alamat", subtitle: "Jl. Poncol Indah 3 no 78 Lebak Bulus Ciputat Timur, Tangerang Selatan"),
    BiodataViewModel(title: "Kelas", subtitle: "11 IPA B"),
    BiodataViewModel(title: "Status", subtitle: "Aktif")
  ];

  @override
  State createState() => _BiodataState();
}

class _BiodataState extends State<Biodata> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.cBackgroundGrey(),
      appBar: CustomAppBar.backAndTitle(context, "Biodata"),
      body: SingleChildScrollView(
        child: BiodataRowComponent(datas: widget.datas),
      ),
    );
  }
}
