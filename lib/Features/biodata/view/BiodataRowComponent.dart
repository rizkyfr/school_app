import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';

class BiodataRowComponent extends StatefulWidget {
  final List<BiodataViewModel> datas;

  const BiodataRowComponent({required this.datas});

  @override
  State createState() {
    return _BiodataRowComponent();
  }
}

class _BiodataRowComponent extends State<BiodataRowComponent> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 20),
          child: Column(
            children: [
              Stack(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 3.0,
                        height: 30.0,
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: CustomColor.cGreen()),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 20),
                    alignment: Alignment.center,
                    child: Image(
                      image: AssetImage("assets/images/ic_profile.png"),
                    ),
                  ),
                ],
              ),
              Column(
                children: widget.datas
                    .asMap()
                    .map(
                      (key, value) {
                        return MapEntry(
                          key,
                          Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 12, right: 12, top: 0, bottom: 6),
                                      child: Text(
                                        value.title,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: CustomColor.cGreyTextLight(),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  if (key < widget.datas.length - 1)
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 15, right: 15, top: 3, bottom: 24),
                                        child: Text(
                                          value.subtitle,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: CustomColor.cGreyText(),
                                          ),
                                        ),
                                      ),
                                    )
                                  else
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 15, right: 15, top: 3, bottom: 0),
                                        child: Text(
                                          value.subtitle,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: CustomColor.cGreyText(),
                                          ),
                                        ),
                                      ),
                                    )
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    )
                    .values
                    .toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
