import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';

class ProfileHeaderComponent extends StatefulWidget {
  final ProfileHeaderViewModel profileHeaderViewModel;
  const ProfileHeaderComponent({required this.profileHeaderViewModel});

  @override
  State createState() {
    return _ProfileHeaderComponent();
  }
}

class _ProfileHeaderComponent extends State<ProfileHeaderComponent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Image(image: AssetImage(widget.profileHeaderViewModel.image)),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Text(widget.profileHeaderViewModel.name, style: TextStyle(fontSize: 17, color: CustomColor.cGreyText(), fontWeight: FontWeight.w700)),
              SizedBox(height: 5),
              Text(widget.profileHeaderViewModel.school, style: TextStyle(fontSize: 17, color: CustomColor.cGreyText())),
              SizedBox(height: 5),
              Text("${widget.profileHeaderViewModel.grade} - ${widget.profileHeaderViewModel.semester}",
                  style: TextStyle(fontSize: 17, color: CustomColor.cGreyText()))
            ],
          ),
        ],
      ),
    );
  }
}
