import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/biodata/view/Biodata.dart';
import 'package:school_app/Features/dokumen/view/Dokumen.dart';
import 'package:school_app/Features/profile/view/ProfileHeaderComponent.dart';
import 'package:school_app/Features/profile/view/ProfileRowComponent.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';
import 'package:school_app/WIP.dart';

class Profile extends StatefulWidget {
  @override
  State createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  List<ProfileViewRowContent> dataSiswa = [
    ProfileViewRowContent(name: "Biodata", widget: Biodata()),
    ProfileViewRowContent(name: "Dokumen", widget: Dokumen()),
    ProfileViewRowContent(name: "Laporan Absensi", widget: WIP()),
    ProfileViewRowContent(name: "Penilaian", widget: WIP()),
  ];

  List<ProfileViewRowContent> lainnya = [
    ProfileViewRowContent(name: "FAQ", widget: WIP()),
    ProfileViewRowContent(name: "Keluar", widget: WIP()),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.cBackgroundGrey(),
      appBar: CustomAppBar.titleAndNotif("School App"),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              ProfileHeaderComponent(
                profileHeaderViewModel: ProfileHeaderViewModel(
                    image: "assets/images/home_logodummy.png",
                    name: "Aflred Fahd",
                    school: "SMA Keluarga 11 Jakarta",
                    grade: "Kelas 11 IPA B",
                    semester: "Semester Ganjil"),
              ),
              SizedBox(height: 15),
              ProfileRowComponent(viewContents: dataSiswa, title: "Data Siswa", color: CustomColor.cRed()),
              SizedBox(height: 15),
              ProfileRowComponent(viewContents: lainnya, title: "Lainnya", color: CustomColor.cBlue()),
              SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }
}
