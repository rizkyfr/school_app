import 'package:flutter/material.dart';

class ProfileViewRowContent {
  final String name;
  final StatefulWidget widget;

  const ProfileViewRowContent({required this.name, required this.widget});
}

class BiodataViewModel {
  final String title;
  final String subtitle;

  const BiodataViewModel({required this.title, required this.subtitle});
}

class ProfileHeaderViewModel {
  final String image;
  final String name;
  final String school;
  final String grade;
  final String semester;

  const ProfileHeaderViewModel({required this.image, required this.name, required this.school, required this.grade, required this.semester});
}