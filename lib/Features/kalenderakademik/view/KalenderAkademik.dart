import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomButton.dart';
import 'package:school_app/Domainless/View/CustomCalendar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/kalenderakademik/view/KalenderRowComponent.dart';
import 'package:school_app/Features/kalenderakademik/viewmodel/KalenderViewContent.dart';

class KalenderAkademik extends StatefulWidget {
  @override
  _KalenderAkademikState createState() => _KalenderAkademikState();
}

class _KalenderAkademikState extends State<KalenderAkademik> {
  List<String> _years = ['2020/2021', '2021/2022', '2022/2023'];
  String _selectedYears = '2020/2021';
  List<String> _semesters = ['Ganjil', 'Genap'];
  String _selectedSemester = 'Ganjil';
  List<String> _months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  String _selectedMonth = 'Juli';

  List<KalenderViewContent> dataKegiatan = [
    KalenderViewContent(title: "Hari Pertama Awal Tahun Pelajaran", subtitle: "13 Juli 2020"),
    KalenderViewContent(title: "Masa Pengenalan Lingkungan Sekolah (MPLS) Kelas X", subtitle: "13 Juli 2020 - 15 Juli 2020"),
    KalenderViewContent(title: "Belajar Efektif", subtitle: "13 Juli 2020 - 30 Juli 2020")
  ];

  List<KalenderViewContent> dataLibur = [
    KalenderViewContent(title: "", subtitle: "Tidak ada jadwal libur"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.backAndTitle(context, "Kalender Akademik"),
      body: SingleChildScrollView(
        child: Container(
          
          color: CustomColor.cBackgroundGrey(),
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Row(
                        children: [
                          Expanded(child: dropDownYears()),
                          SizedBox(width: 20),
                          Expanded(child: dropDownSemester()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        children: [
                          Expanded(child: dropDownMonth()),
                          SizedBox(width: 20),
                          SvgPicture.asset("assets/images/ic_kalender_biru.svg"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, bottom: 20),
                      child: CustomCalendar(),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 20, right: 15),
                child: KalenderRowComponent(viewContents: dataKegiatan, title: "Kegiatan Akademik", color: CustomColor.cGreen()),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 20, right: 15, bottom: 20),
                child: KalenderRowComponent(viewContents: dataLibur, title: "Hari Libur Nasional", color: CustomColor.cBlue()),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: ButtonStaticOutline(
                  text: "Download PDF Kalender Akademik",
                  onTap: () {},
                  color: CustomColor.cRed(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dropDownYears() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Tahun Akademik",
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
        ),
        SizedBox(height: 10),
        Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1),
          ),
          child: InputDecorator(
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
              contentPadding: EdgeInsets.all(10),
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                icon: Icon(Icons.keyboard_arrow_down),
                value: _selectedYears,
                isDense: true,
                isExpanded: true,
                items: _years.map((years) {
                  return DropdownMenuItem(
                    child: new Text(years),
                    value: years,
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _selectedYears = newValue.toString();
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget dropDownSemester() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Semester",
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
        ),
        SizedBox(height: 10),
        Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1),
          ),
          child: InputDecorator(
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
              contentPadding: EdgeInsets.all(10),
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                icon: Icon(Icons.keyboard_arrow_down),
                value: _selectedSemester,
                isDense: true,
                isExpanded: true,
                items: _semesters.map((semester) {
                  return DropdownMenuItem(
                    child: new Text(semester),
                    value: semester,
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _selectedSemester = newValue.toString();
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget dropDownMonth() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1),
          ),
          child: InputDecorator(
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
              contentPadding: EdgeInsets.all(10),
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                icon: Icon(Icons.keyboard_arrow_down),
                value: _selectedMonth,
                isDense: true,
                isExpanded: true,
                items: _months.map((month) {
                  return DropdownMenuItem(
                    child: new Text(month),
                    value: month,
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _selectedMonth = newValue.toString();
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
