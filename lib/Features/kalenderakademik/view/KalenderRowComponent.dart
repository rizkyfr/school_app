import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/kalenderakademik/viewmodel/KalenderViewContent.dart';

class KalenderRowComponent extends StatefulWidget {
  final List<KalenderViewContent> viewContents;
  final String title;
  final Color color;

  KalenderRowComponent({required this.viewContents, required this.title, required this.color});

  @override
  State createState() {
    return _KalenderRowComponent();
  }
}

class _KalenderRowComponent extends State<KalenderRowComponent> {
  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              SizedBox(
                width: 3.0,
                height: 30.0,
                child: DecoratedBox(
                  decoration: BoxDecoration(color: widget.color),
                ),
              ),
              SizedBox(width: 12),
              Text(widget.title, style: TextStyle(fontSize: 16, color: CustomColor.cGreyText(), fontWeight: FontWeight.w700))
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12, right: 12, top: 20, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.viewContents
                  .asMap()
                  .map(
                    (i, e) {
                      return MapEntry(
                        i,
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (e.title != "")
                            Padding(
                              padding: EdgeInsets.only(bottom: 5),
                              child: Text(
                                widget.viewContents[i].title,
                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                widget.viewContents[i].subtitle,
                                style: TextStyle(fontSize: 14, color: CustomColor.cGreyTextLight()),
                              ),
                            ),
                            if (i < widget.viewContents.length - 1)
                              Padding(
                                padding: const EdgeInsets.only(left: 0, right: 0, top: 25, bottom: 25),
                                child: SizedBox(
                                  height: 1,
                                  width: MediaQuery.of(context).size.width,
                                  child: DecoratedBox(
                                    decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      );
                    },
                  )
                  .values
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
