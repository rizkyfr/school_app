class KalenderViewContent {
  final String title;
  final String subtitle;

  const KalenderViewContent({required this.title, required this.subtitle});
}