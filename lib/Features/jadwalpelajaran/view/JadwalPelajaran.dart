import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Features/jadwalpelajaran/view/JadwalPelajaranComponent.dart';
import 'package:school_app/Features/jadwalpelajaran/viewmodel/JadwalPelajaranViewContent.dart';

class JadwalPelajaran extends StatefulWidget {
  @override
  State createState() => _JadwalPelajaranState();
}

class _JadwalPelajaranState extends State<JadwalPelajaran> {
  int _value = 0;
  String _valueName = "Senin";

  List<JadwalPelajaranViewContent> dataJadwal = [
    JadwalPelajaranViewContent(title: "Pendidikan Agama Islam", subtitle: "07.00 - 08.30"),
    JadwalPelajaranViewContent(title: "PKN", subtitle: "08.45 - 10.00"),
    JadwalPelajaranViewContent(title: "Bahasa Indonesia", subtitle: "11.00 - 12.00"),
    JadwalPelajaranViewContent(title: "Bahasa Inggris", subtitle: "13.00 - 14.00")
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.backAndTitle(context, "Jadwal Pelajaran"),
      body: SingleChildScrollView(
        child: Container(
          color: CustomColor.cBackgroundGrey(),
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: _value == 0
                              ? BoxDecoration(borderRadius: BorderRadius.circular(10), color: CustomColor.cBackgroundRed())
                              : BoxDecoration(
                                  borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 0;
                                _valueName = "Senin";
                              });
                            },
                            child: Text(
                              "Senin",
                              style: TextStyle(color: _value == 0 ? CustomColor.cRed() : Colors.black),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: _value == 1
                              ? BoxDecoration(borderRadius: BorderRadius.circular(10), color: CustomColor.cBackgroundRed())
                              : BoxDecoration(
                                  borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 1;
                                _valueName = "Selasa";
                              });
                            },
                            child: Text(
                              "Selasa",
                              style: TextStyle(color: _value == 1 ? CustomColor.cRed() : Colors.black),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: _value == 2
                              ? BoxDecoration(borderRadius: BorderRadius.circular(10), color: CustomColor.cBackgroundRed())
                              : BoxDecoration(
                                  borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 2;
                                _valueName = "Rabu";
                              });
                            },
                            child: Text(
                              "Rabu",
                              style: TextStyle(color: _value == 2 ? CustomColor.cRed() : Colors.black),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: _value == 3
                              ? BoxDecoration(borderRadius: BorderRadius.circular(10), color: CustomColor.cBackgroundRed())
                              : BoxDecoration(
                                  borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 3;
                                _valueName = "Kamis";
                              });
                            },
                            child: Text(
                              "Kamis",
                              style: TextStyle(color: _value == 3 ? CustomColor.cRed() : Colors.black),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: _value == 4
                              ? BoxDecoration(borderRadius: BorderRadius.circular(10), color: CustomColor.cBackgroundRed())
                              : BoxDecoration(
                                  borderRadius: BorderRadius.circular(10), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _value = 4;
                                _valueName = "Jumat";
                              });
                            },
                            child: Text(
                              "Jumat",
                              style: TextStyle(color: _value == 4 ? CustomColor.cRed() : Colors.black),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, right: 15, left: 15),
                child: JadwalPelajaranComponent(viewContents: dataJadwal, title: _valueName, color: CustomColor.cGreen()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
