class JadwalPelajaranViewContent {
  final String title;
  final String subtitle;

  const JadwalPelajaranViewContent({required this.title, required this.subtitle});
}