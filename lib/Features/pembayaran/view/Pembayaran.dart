import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school_app/Domainless/View/CustomAppBar.dart';
import 'package:school_app/Domainless/View/CustomButton.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Domainless/View/TwoSideText.dart';
import 'package:school_app/Features/home/view/NotifStatusBayaranPage.dart';
import 'package:school_app/Features/profile/view/ProfileHeaderComponent.dart';
import 'package:school_app/Features/profile/viewmodel/ProfileViewModel.dart';

class Pembayaran extends StatefulWidget {
  @override
  State createState() => _PembayaranState();
}

class _PembayaranState extends State<Pembayaran> {
  int _value = 0;
  
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.cBackgroundGrey(),
      appBar: CustomAppBar.titleAndNotif("School App"),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              ProfileHeaderComponent(
                profileHeaderViewModel: ProfileHeaderViewModel(
                    image: "assets/images/home_logodummy.png",
                    name: "Aflred Fahd",
                    school: "SMA Keluarga 11 Jakarta",
                    grade: "Kelas 11 IPA B",
                    semester: "Semester Ganjil"),
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                padding: EdgeInsets.only(top: 15, bottom: 20),
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: [
                            SizedBox(height: 10),
                            Row(
                              children: [
                                SizedBox(
                                  width: 3.0,
                                  height: 30.0,
                                  child: DecoratedBox(
                                    decoration: BoxDecoration(color: CustomColor.cGreen()),
                                  ),
                                ),
                                SizedBox(width: 12),
                                Expanded(
                                  child: Text("Payment List",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CustomColor.cGreyText())),
                                ),
                                GestureDetector(
                                  child: Text("History Pembayaran",
                                      style: TextStyle(color: CustomColor.cRed(), fontSize: 13, fontWeight: FontWeight.w600)),
                                  onTap: () {},
                                ),
                                SizedBox(width: 20),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                      decoration: _value == 0
                                          ? BoxDecoration(borderRadius: BorderRadius.circular(30), color: CustomColor.cBackgroundRed())
                                          : BoxDecoration(
                                              borderRadius: BorderRadius.circular(30),
                                              border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _value = 0;
                                          });
                                        },
                                        child: Text(
                                          "Yang Harus Dibayar",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 13,color: _value == 0 ? CustomColor.cRed() : Colors.black),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width:10),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                      decoration: _value == 1
                                          ? BoxDecoration(borderRadius: BorderRadius.circular(30), color: CustomColor.cBackgroundRed())
                                          : BoxDecoration(
                                              borderRadius: BorderRadius.circular(30),
                                              border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _value = 1;
                                          });
                                        },
                                        child: Text(
                                          "Konfirmasi Pembayaran",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 13, color: _value == 1 ? CustomColor.cRed() : Colors.black),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      _value == 0
                          ? NotifStatusBayaranPage(
                              status: "tidak",
                            )
                          : paymentSuccess(context),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget paymentSuccess(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(bottom: 20),
    padding: EdgeInsets.only(top: 20, bottom: 20, right: 15, left: 15),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(5),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SvgPicture.asset("assets/images/ic_success.svg", height: 55, width: 55),
        SizedBox(height: 20),
        Text("Terima Kasih", textAlign: TextAlign.center, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: CustomColor.cGreen())),
        SizedBox(height: 10),
        Text("Pembayaran SPP bulan September sudah\nterbayarkan",
            textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: CustomColor.cGreyText())),
        Padding(
          padding: const EdgeInsets.only(top: 15, bottom: 10),
          child: SizedBox(
            height: 1,
            width: MediaQuery.of(context).size.width,
            child: DecoratedBox(
              decoration: BoxDecoration(color: CustomColor.cBackgroundGrey()),
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Detail Pembayaran",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(height: 10),
        TwoSideText("Nama Murid", "Alfred Fahd", context: context,),
        SizedBox(height: 10),
        TwoSideText("Total", "Rp 100.000", context: context,),
        SizedBox(height: 10),
        TwoSideText("Kode", "SPP September 2021", context: context,),
        SizedBox(height: 20),
        ButtonStatic(
          text: "Lanjut",
          color: CustomColor.cGreen(),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ],
    ),
  );
}
