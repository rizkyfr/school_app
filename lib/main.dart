import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:school_app/Features/home/view/HomeTabPage.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  
  runApp(SchoolApp());
}

class SchoolApp extends StatefulWidget {
  @override
  SchoolAppState createState() => SchoolAppState();
}

class SchoolAppState extends State<SchoolApp> {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "School App",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        initialRoute: "homeTabPage",
        routes: {
          // "/splash": (context) => SplashScreenPage(),
          // "/login": (context) => LoginPage(),
          // "landing": (context) => LandingPage(),
          "homeTabPage": (context) => HomeTabPage(),
        },
    );
  }
}
