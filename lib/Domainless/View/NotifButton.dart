import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotifButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 60,
      child: Stack(
        children: <Widget>[
          FlatButton(
            padding: EdgeInsets.all(0),
            child: SvgPicture.asset("assets/images/ic_notif.svg"),
            onPressed: () {
            },
          ),
        ],
      ),
    );
  }
}
