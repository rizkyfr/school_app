import 'package:flutter/material.dart';

extension CustomColor on Colors {
  static Color cGreyText() {
    return Color.fromRGBO(51, 51, 51, 1);
  }

  static Color cGreyTextLight() {
    return Color.fromRGBO(51, 51, 51, 99);
  }

  static Color cBackgroundGrey() {
    return Color.fromRGBO(243, 243, 243, 1);
  }

  static Color cRed() {
    return Color.fromRGBO(226, 6, 32, 1);
  }

  static Color cBackgroundRed() {
    return Color.fromRGBO(254, 225, 229, 1);
  }

  static Color cBlue() {
    return Color.fromRGBO(72, 146, 204, 1);
  }

  static Color cBackgroundBlue() {
    return Color.fromRGBO(231, 241, 248, 1);
  }

  static Color cGreen() {
    return Color.fromRGBO(77, 171, 140, 1);
  }

  static Color cBackgroundGreen() {
    return Color.fromRGBO(227, 240, 209, 1);
  }
}
