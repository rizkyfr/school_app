import 'package:flutter/material.dart';

abstract class ButtonComponent extends StatefulWidget {
  final String text;
  final Function() onTap;
  const ButtonComponent({required this.text, required this.onTap});
}

class ButtonStatic extends ButtonComponent {
  final String text;
  final Color color;
  final Function() onTap;
  const ButtonStatic({required this.text, required this.color, required this.onTap}) : super(text: text, onTap: onTap);

  @override
  _ButtonStaticRed createState() {
    return _ButtonStaticRed();
  }
}

class _ButtonStaticRed extends State<ButtonStatic> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            child: TextButton(
              onPressed: widget.onTap,
              child: Text(
                widget.text,
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
              style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.only(top: 15, right: 35, bottom: 15, left: 35)),
                backgroundColor: MaterialStateProperty.all<Color>(widget.color),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ButtonStaticOutline extends ButtonComponent {
  final String text;
  final Color color;
  final Function() onTap;
  const ButtonStaticOutline({required this.text, required this.color, required this.onTap}) : super(text: text, onTap: onTap);

  @override
  _ButtonStaticOutline createState() {
    return _ButtonStaticOutline();
  }
}

class _ButtonStaticOutline extends State<ButtonStaticOutline> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            child: OutlinedButton(
              onPressed: widget.onTap,
              child: Text(
                widget.text,
                style: TextStyle(color: widget.color, fontSize: 16),
              ),
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.only(top: 15, right: 35, bottom: 15, left: 35),
                side: BorderSide(width: 1.0, color: widget.color),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
