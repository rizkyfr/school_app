import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class TwoSideText extends StatelessWidget {
  final String firstText;
  final String secondText;
  final BuildContext context;
  TwoSideText(this.firstText, this.secondText,{required this.context});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Text(
            firstText,
            style: TextStyle(
              fontSize: 14,
              color: CustomColor.cGreyText(),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            secondText,
            style: TextStyle(
              fontSize: 14,
              color: CustomColor.cGreyText(),
              fontWeight: FontWeight.w700
            ),
          ),
        ),
      ],
    );
  }
}