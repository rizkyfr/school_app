import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';

class CustomCalendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<CustomCalendar> {
  DateTime selectedDate = DateTime.now(); // TO tracking date

  int currentDateSelectedIndex = 0; //For Horizontal Date
  ScrollController scrollController = ScrollController(); //To Track Scroll of ListView

  List<String> listOfMonths = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];

  List<String> listOfDays = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"];

  @override
  void initState() {
    currentDateSelectedIndex = DateUtils.getDaysInMonth(selectedDate.year, selectedDate.month)-1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var daysInMonth = DateUtils.getDaysInMonth(selectedDate.year, selectedDate.month);
    return Column(
      children: [
        Container(
          height: 38,
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(width: 10);
            },
            itemCount: daysInMonth,
            controller: scrollController,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  setState(() {
                    currentDateSelectedIndex = index;
                    selectedDate = DateTime.utc(selectedDate.year, selectedDate.month, 1).add(Duration(days: index));
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: currentDateSelectedIndex == index
                      ? BoxDecoration(borderRadius: BorderRadius.circular(30), color: CustomColor.cBackgroundRed())
                      : BoxDecoration(borderRadius: BorderRadius.circular(30), border: Border.all(color: CustomColor.cBackgroundGrey(), width: 1)),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          listOfDays[DateTime.utc(selectedDate.year, selectedDate.month, 1).add(Duration(days: index)).weekday - 1].toString() +
                              ", " +
                              DateTime.utc(selectedDate.year, selectedDate.month, 1).add(Duration(days: index)).day.toString() +
                              " " +
                              listOfMonths[DateTime.utc(selectedDate.year, selectedDate.month, 1).add(Duration(days: index)).month - 1].toString(),
                          style: TextStyle(fontSize: 16, color: currentDateSelectedIndex == index ? CustomColor.cRed() : CustomColor.cGreyText()),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
