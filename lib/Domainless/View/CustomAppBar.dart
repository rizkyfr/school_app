import 'package:flutter/material.dart';
import 'package:school_app/Domainless/View/CustomColor.dart';
import 'package:school_app/Domainless/View/NotifButton.dart';

class CustomAppBar {
  static AppBar titleAndNotif(String title) {
    return AppBar(
      title: Text(title, style: TextStyle(color: Color(0xff333333), fontWeight: FontWeight.w700)),
      actions: <Widget>[NotifButton()],
      centerTitle: false,
      backgroundColor: Colors.white,
      brightness: Brightness.light,
      elevation: 0.3,
    );
  }

  static AppBar backAndTitle(context, String title) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: Container(
        margin: EdgeInsets.only(left: 10),
        child: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: CustomColor.cRed(),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      title: Text(
        title,
        style: TextStyle(color: CustomColor.cGreyText(), fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'FiraSans'),
      ),
      centerTitle: false,
      brightness: Brightness.dark,
      elevation: 0,
    );
  }
}
